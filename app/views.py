from app import app
from flask import render_template
from app import rly, channel
import json

@app.route('/')
@app.route('/index')

def index ():
    display = []
    num = 1
    for item in channel :
        btn_color = "btn-default"
        if rly.getStatus(num):
            btn_color = "btn-danger"
        info ={"number":num,"color":btn_color,"bdid":item}
        num += 1
        display.append(info)
    return  render_template("index.html",relay = display)

@app.route('/status/<index>')
def status(index):
    js = json.dumps({'number':int(index),'status':rly.getStatus(int(index))})
    return str(js)

@app.route('/off/<index>')
def off(index):
    print index
    rly.on(int(index))
    return "ok"

@app.route('/switch/<int:index>')
def switch(index):
    rly.switch(index)
    return str(json.dumps({'number':int(index),'status':rly.getStatus(int(index))}))
