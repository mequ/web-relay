#! /usr/bin/env python 
from flask import Flask
from relay import Relay

channel = [16,11,18,22,36,15,13,37]
rly = Relay(channel,init=True)
#rly = None
app = Flask(__name__)
from app import views
