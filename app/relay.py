import RPi.GPIO as GPIO
from time import sleep

class Relay:
    def __init__(self,keys,init=False):
        self.channel = keys
        self.status  = {}
        for key in self.channel:
            self.status[key] = init
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.channel,GPIO.OUT,initial =  not init)

    def all(self,stat):
        GPIO.output(self.channel,not stat)
        for item in self.status:
            self.status[item] = stat

    def on(self,id):
        item = self.channel[id-1]
        self.__output(item,True)

    def off(self,id):
        item = self.channel[id-1]
        self.__output(item,False)

    def __output(self,key,stat):
        GPIO.output(key,not stat)
        self.status[key] = stat 

    def switch(self,id):
        item =  self.channel[id-1]
        stat = not self.status[item]
        self.__output(item,stat)

    def secunce(self,delay=1,stat = True,reverse=False):
        mychannel = list(self.channel)
        
        if reverse :
            mychannel.reverse()

        for item in mychannel:
            self.__output(item,stat)
            sleep(delay)

    def getStatus(self,id):
        key = self.channel[id-1]
        return self.status[key]
        
    def __del__(self):
        try:
            GPIO.cleanup(self.channel)
        except Exception:
            raise
    
    
    
    
    


if __name__=="__main__":
    relay = Relay([16,11,18,22,36,15,13,37])
    #relay.allSwitch()
    print relay.status
    #relay.allSwitch(False)
    sleep(5)
    while True:
        relay.secunce()
        relay.secunce(stat = False,reverse=True)
    print relay.status
    sleep(5) 
